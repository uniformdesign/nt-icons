module.exports = function(grunt) {
  // Project configuration.
  grunt.initConfig({
  pkg: grunt.file.readJSON('package.json'),

  clean: [
    "tmp",
    "build"
  ],

  svgmin: {
    options: {
      plugins: [
        { removeViewBox: true }, // removes the viewbox atribute from the SVG
        { removeUselessStrokeAndFill: true }, // removes Useless Strokes and Fills
        { removeEmptyAttrs: true } // removes Empty Attributes from the SVG
      ]
    },
    dist: {
      files: [{
        expand: true,
        cwd: 'icons',
        src: ['**/*.svg'],
        dest: 'tmp/',
        ext: '.svg'
      }]
    }
  },

  webfont: {
    icons: {
        src: 'tmp/**/*.svg',
        dest: 'build',
        destCss: 'build/css',
        options: {
          engine: 'node',
          font: '<%= pkg.name %>',
          hashes: true, //Append font file names with unique string to flush browser cache
          style: 'font,icon,extra',
          htmlDemoTemplate: 'templates/demo.html',
          stylesheet: 'less',
          autoHint: false,
          //fontHeight: '2056', //The output font height, Default: 512
          //descent: '512', // The descent should be a positive value. The ascent formula is: ascent = fontHeight - descent, Default: 64
          templateOptions: {
            baseClass: 'icon',
            classPrefix: 'icon-',
            mixinPrefix: 'icon-'
          }
        }
      }
    }
  });

  // Load tasks
  grunt.loadNpmTasks('grunt-svgmin');
  grunt.loadNpmTasks('grunt-webfont');
  grunt.loadNpmTasks('grunt-contrib-clean');

  // Register tasks
  grunt.registerTask('default',[
    'clean',
    'svgmin',
    'webfont'
  ]);

};